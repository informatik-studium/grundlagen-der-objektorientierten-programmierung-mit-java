package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.kassensimulation.FifoKassierStrategie;
import org.iu.dsgopj102101.uebungsprojekt.kassensimulation.IKassierStrategie;
import org.iu.dsgopj102101.uebungsprojekt.kassensimulation.SifoKassierStrategie;

public class KassenSimulation {

    public static void main(String[] args) {
        int numKassen = 4;
        int numEinkaufswaegen = 10;
        int maxNumArtikel = 50;

        IKassierStrategie sifo = new SifoKassierStrategie();
        sifo.start(numKassen, numEinkaufswaegen, maxNumArtikel);
        System.out.println(sifo.print());

        IKassierStrategie fifo = new FifoKassierStrategie();
        fifo.start(numKassen, numEinkaufswaegen, maxNumArtikel);
        System.out.println(fifo.print());
    }

}
