package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.iotools.IOTools;
import org.iu.dsgopj102101.uebungsprojekt.spiel.*;

public class Spiel {

    public static void main(String[] args) {
        Spieler spieler = new Spieler("LegendaryGamer2014");
        while (true) {
            String eingabe = IOTools.readString("Welches Objekt möchten Sie benutzen? ");

            Spielobjekt spielobjekt = null;
            switch (eingabe.toLowerCase()) {
                case "buch":
                    spielobjekt = new Buch(spieler);
                    break;
                case "lampe":
                    spielobjekt = new Lampe(spieler);
                    break;
                case "abendessen":
                    spielobjekt = new Abendessen(spieler);
                    break;
            }

            if (spielobjekt != null) {
                spielobjekt.benutzen();
            } else {
                System.out.println("Ungültiges Spielobjekt (gültig: Buch, Lampe, Abendessen)");
            }
        }
    }
}
