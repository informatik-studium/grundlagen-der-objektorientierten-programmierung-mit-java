package org.iu.dsgopj102101.uebungsprojekt.lagermanagement;

import java.util.concurrent.LinkedBlockingQueue;

public class SiloTurm<E> extends LinkedBlockingQueue<E> implements Lagerplatz<E> {

    @Override
    public void einlagern(E e) {
        this.add(e);
    }

}
