package org.iu.dsgopj102101.uebungsprojekt.lagermanagement;

public class Artikel {

    private String name;
    private int nummer;

    public Artikel(String name, int nummer) {
        this.name = name;
        this.nummer = nummer;
    }

    public String getName() {
        return name;
    }

    public int getNummer() {
        return nummer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

}
