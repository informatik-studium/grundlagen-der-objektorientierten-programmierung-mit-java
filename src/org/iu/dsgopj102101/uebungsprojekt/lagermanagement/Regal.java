package org.iu.dsgopj102101.uebungsprojekt.lagermanagement;

import java.util.ArrayList;

public class Regal<E> extends ArrayList<E> implements Lagerplatz<E> {

    @Override
    public void einlagern(E e) {
        this.add(e);
    }

}
