package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.lagermanagement.*;
import org.iu.dsgopj102101.uebungsprojekt.iotools.IOTools;

public class Lagermanagement {

    public static void main(String[] args) {
        SchuettgueterFlaeche<Artikel> schuettgueterFlaeche = new SchuettgueterFlaeche<>();
        Regal<Artikel> regal = new Regal<>();
        SiloTurm<Artikel> siloTurm = new SiloTurm<>();

        while (true) {
            System.out.println("------------------------------------------------------");
            System.out.println("Es gibt die folgenden Lager: (1) Schüttgüterfläche, (2) Regal, (3) Silo-Turm");
            int lager = IOTools.readInteger("In welches Lager möchten Sie Artikel einlagern? (1-3) ");
            if (lager < 1 || lager > 3) {
                System.out.println("Ungültiges Lager!");
                continue;
            }

            System.out.print("Gewähltes Lager: ");
            Lagerplatz<Artikel> lagerplatz;
            if (lager == 1) {
                lagerplatz = schuettgueterFlaeche;
                System.out.println("Schüttgüterfläche");
            } else if (lager == 2) {
                lagerplatz = regal;
                System.out.println("Regal");
            } else {
                lagerplatz = siloTurm;
                System.out.println("Silo-Turm");
            }

            String name = IOTools.readString("Wie heißt der Artikel? ");
            int nummer = IOTools.readInteger("Welche Nummer hat der Artikel? ");
            int menge = IOTools.readInteger("Wie viele Artikel möchten Sie einlagern? ");

            for (int i = 0; i < menge; i++) {
                Artikel artikel = new Artikel(name, nummer);
                lagerplatz.einlagern(artikel);
            }
            System.out.printf("Artikel \"%s\" mit Nummer %d %d Mal eingelagert.%n", name, nummer, menge);
        }
    }
}
