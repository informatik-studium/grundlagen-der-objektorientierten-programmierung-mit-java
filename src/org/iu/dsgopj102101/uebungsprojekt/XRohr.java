package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.xrohr.Rohr;

public class XRohr {

    public static void main(String[] args) {
        Rohr rohr = new Rohr(10, 5, 2);
        System.out.println(rohr.berechneVolumen());
    }
}
