package org.iu.dsgopj102101.uebungsprojekt.klausur.fahrtenbuch;

import java.util.Date;

public class Fahrzeug implements IFahrtenbuchObjekt {

    private String seriennummer;
    private String kennzeichen;
    private Date erstzulassung;
    private float kilometerstand;

    public Fahrzeug(String seriennummer, String kennzeichen, Date erstzulassung, float kilometerstand) {
        this.seriennummer = seriennummer;
        this.kennzeichen = kennzeichen;
        this.erstzulassung = erstzulassung;
        this.kilometerstand = kilometerstand;
    }

    @Override
    public String getSeriennummer() {
        return seriennummer;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public Date getErstzulassung() {
        return erstzulassung;
    }

    public float getKilometerstand() {
        return kilometerstand;
    }

    // TODO: Setter

    @Override
    public String toString() {
        return "Fahrzeug{" + "seriennummer=" + seriennummer + ", kennzeichen=" + kennzeichen + ", erstzulassung=" + erstzulassung + ", kilometerstand=" + kilometerstand + "}";
    }
}
