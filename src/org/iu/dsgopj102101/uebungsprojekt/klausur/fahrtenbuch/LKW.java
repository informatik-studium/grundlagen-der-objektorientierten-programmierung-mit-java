package org.iu.dsgopj102101.uebungsprojekt.klausur.fahrtenbuch;

import java.util.Date;

public class LKW extends Fahrzeug {

    private float maxZuglast;
    private float radstand;

    public LKW(String seriennummer, String kennzeichen, Date erstzulassung, float kilometerstand, float maxZuglast, float radstand) {
        super(seriennummer, kennzeichen, erstzulassung, kilometerstand);
        this.maxZuglast = maxZuglast;
        this.radstand = radstand;
    }

    // TODO: Getter und Setter

    @Override
    public String toString() {
        return "LKW{" + "seriennummer=" + getSeriennummer() + ", kennzeichen=" + getKennzeichen() + ", erstzulassung=" + getErstzulassung() + ", kilometerstand=" + getKilometerstand() + ", maxZuglast=" + maxZuglast + ", radstand=" + radstand + "}";
    }
}
