package org.iu.dsgopj102101.uebungsprojekt.klausur.fahrtenbuch;

import org.iu.dsgopj102101.uebungsprojekt.klausur.protokollierung.IProtocolEntry;
import org.iu.dsgopj102101.uebungsprojekt.klausur.protokollierung.Person;

import java.util.Date;

public class FahrtenbuchEintrag implements IProtocolEntry {

    private Person person;
    private IFahrtenbuchObjekt objekt;
    private Date start;
    private Date ende;
    private String beschreibung;

    public FahrtenbuchEintrag(Person person, IFahrtenbuchObjekt objekt, Date start, Date ende, String beschreibung) {
        this.person = person;
        this.objekt = objekt;
        this.start = start;
        this.ende = ende;
        this.beschreibung = beschreibung;
    }

    @Override
    public Person getPerson() {
        return person;
    }

    @Override
    public Object getObject() {
        return objekt;
    }

    @Override
    public Date getDateTimeFrom() {
        return start;
    }

    @Override
    public Date getDateTimeTo() {
        return ende;
    }

    @Override
    public String getDescription() {
        return beschreibung;
    }
}
