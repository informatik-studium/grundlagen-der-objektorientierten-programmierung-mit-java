package org.iu.dsgopj102101.uebungsprojekt.klausur.fahrtenbuch;

public class Auflieger implements IFahrtenbuchObjekt {

    private String seriennummer;
    private String kennzeichen;

    public Auflieger(String seriennummer, String kennzeichen) {
        this.seriennummer = seriennummer;
        this.kennzeichen = kennzeichen;
    }

    @Override
    public String getSeriennummer() {
        return seriennummer;
    }

    // TODO: Getter und Setter

    @Override
    public String toString() {
        return "Auflieger{" + "seriennummer=" + seriennummer + ", kennzeichen=" + kennzeichen + "}";
    }
}
