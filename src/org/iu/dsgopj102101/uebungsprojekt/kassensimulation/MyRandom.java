package org.iu.dsgopj102101.uebungsprojekt.kassensimulation;

public class MyRandom {

    public static int randomInt(int pMin, int pMax) {
        int min = 0;
        int max = pMax - pMin;
        double d = ((Math.random() / (min + 1.0 / (max)) + 2.0)) - 2;
        d = d + pMin;
        return (int) d;
    }
}
