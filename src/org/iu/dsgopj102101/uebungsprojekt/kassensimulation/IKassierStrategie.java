package org.iu.dsgopj102101.uebungsprojekt.kassensimulation;

public interface IKassierStrategie {

    /**
     * @return Name der Strategie
     */
    public String name();

    /**
     * Startet die Ausführung (Kassiervorgang). Hinweis: 1) Nach dem Start sollte
     * die Strategie initialisiert werden 1.1) Eine Liste von Einkaufswagen
     * (=Schlange) kann durch die EinkaufswagenFactory erzeugt werden). 1.2) Je
     * Strategie sollten Kassen erzeugt werden. Die Strategie regelt die Zuordnung
     * von Kassen und Schlangen. 2) Nach der Initialisierung sollte das Kassieren
     * beginnen. 2.1) Einkaufswagen der Schlange sollten den Kassen zugeordnet
     * werden. 2.2) An den Kassen steht die Methode kassieren zur Verfügung.
     *
     * @param numKassen         - Anzahl Kassen
     * @param numEinkaufswaegen - Anzahl Einkaufswägen
     * @param numMaxArtikel     - max. Artikel pro Einkaufswagen
     */
    public void start(int numKassen, int numEinkaufswaegen, int numMaxArtikel);

    /**
     * Berechnet die mittlere Wartezeit.
     *
     * @return Mittlere Wartezeit in Minuten
     */
    public double getMittlereWartezeit();

    /**
     * Ausgabe des Ergebnisses als String.
     *
     * @return
     */
    default String print() {
        return name() + " erreicht eine mittlere Wartezeit von " + getMittlereWartezeit() + " Sekunden";
    }
}
