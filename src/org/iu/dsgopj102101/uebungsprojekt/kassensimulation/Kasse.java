package org.iu.dsgopj102101.uebungsprojekt.kassensimulation;

public class Kasse {

    private long bearbeitungsZeit;
    private long geblocktFuer;

    public Kasse() {
        this.bearbeitungsZeit = 0;
    }

    /**
     * Kassiert einen Einkaufswagen ab. Beim Kassieren wird dem Einkaufswagen seine
     * Wartezeit auf Basis der bisherigen Bearbeitungszeit der Kasse zugeordnet.
     *
     * @param ew - Einkaufswagen der abkassiert wird.
     */
    public void kassieren(Einkaufswagen ew) {
        long dauer = getDauer(ew);
        ew.setWartezeit(this.bearbeitungsZeit);
        this.bearbeitungsZeit += dauer;
        this.setGeblocktFuer(dauer);
    }

    /**
     * Bearbeitungszeit in Sekunden.
     */
    private long getDauer(Einkaufswagen ew) {
        return ew.getNumArtikel() * 10 + 120;
    }

    /**
     * Je Kasse ist bekannt, wie lang die Kasse zurzeit geblockt ist.
     *
     * @return Anz. Sekunden die die Kasse geblockt ist.
     */
    public long getGeblocktFuer() {
        return this.geblocktFuer;
    }

    private void setGeblocktFuer(long geblocktFuer) {
        if (geblocktFuer >= 0) {
            this.geblocktFuer = geblocktFuer;
        }
    }
}
