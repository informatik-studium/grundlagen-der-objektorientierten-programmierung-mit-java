package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.terminueberwachung.TeamKalender;

import java.io.IOException;

public class Terminueberwachung {

    public static void main(String[] args) {
        TeamKalender teamKalender = new TeamKalender();

        try {
            teamKalender.importFromCsv("src/org/iu/dsgopj102101/uebungsprojekt/terminueberwachung/Terminliste.csv");
            System.out.println(teamKalender);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
