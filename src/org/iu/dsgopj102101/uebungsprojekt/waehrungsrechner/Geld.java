package org.iu.dsgopj102101.uebungsprojekt.waehrungsrechner;

public class Geld {

    private final Waehrung waehrung;
    private final double betrag;

    public Geld(Waehrung waehrung, double betrag) {
        this.waehrung = waehrung;
        this.betrag = betrag;
    }

    public Waehrung getWaehrung() {
        return waehrung;
    }

    public double getBetrag() {
        return betrag;
    }

    /**
     * Gibt eine String-Repräsentation des Geld-Objekts im Format "(Betrag) (Währungssymbol)" zurück.
     *
     * @return Die String-Repräsentation des Geld-Objekts.
     */
    @Override
    public String toString() {
        return String.format("%.2f %s", betrag, waehrung.getSymbol());
    }

    /**
     * Konvertiert ein Geld-Objekt in ein Geld-Objekt mit einer anderen Währung.
     *
     * @param geld     Das Geld-Objekt, das konvertiert werden soll.
     * @param waehrung Die Währung, in die das Geld-Objekt konvertiert werden soll.
     * @return Das konvertierte Geld-Objekt.
     */
    public static Geld konvertiereZu(Geld geld, Waehrung waehrung) {
        double konvertierterBetrag = geld.getBetrag() * geld.getWaehrung().getEuroBetrag() / waehrung.getEuroBetrag();
        return new Geld(waehrung, konvertierterBetrag);
    }
}
