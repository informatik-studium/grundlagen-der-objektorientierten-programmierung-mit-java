package org.iu.dsgopj102101.uebungsprojekt.datenstruktur;

import java.util.ArrayList;
import java.util.Collection;

public class RandomArrayList<E> extends ArrayList<E> {

    /**
     * Appends the specified element to a random position in the list.
     *
     * @param e element to be randomly added to this list
     * @return {@code true} (as specified by {@link Collection#add})
     */
    @Override
    public boolean add(E e) {
        if (size() == 0) {
            return super.add(e);
        } else {
            add(randomIndex(), e);
            return true;
        }
    }

    /**
     * Removes an element at a random position in the list.
     * Returns null if the list is empty.
     *
     * @return the element that was removed from the list
     */
    public E ziehen() {
        if (size() == 0) {
            return null;
        } else {
            return remove(randomIndex());
        }
    }

    /**
     * Returns a random index in the list.
     * Returns 0 if the list is empty.
     *
     * @return a random index in the list
     */
    private int randomIndex() {
        return (int) (Math.random() * size());
    }
}
