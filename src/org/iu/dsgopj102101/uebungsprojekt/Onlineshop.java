package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.onlineshop.Artikel;
import org.iu.dsgopj102101.uebungsprojekt.onlineshop.Kunde;
import org.iu.dsgopj102101.uebungsprojekt.onlineshop.Verkaeufer;

public class Onlineshop {

    public static void main(String[] args) {
        Verkaeufer lichtAlex = new Verkaeufer("Licht-Alex", "alex.leuchtfunken@gmail.com", "DE153256922");
        Artikel superLampe = new Artikel("Super Lampe 3000", lichtAlex);

        Kunde yogaAnna = new Kunde("Yoga-Anna", "anna@entspannung.com");
        yogaAnna.bestellen(superLampe, 3);
    }

}
