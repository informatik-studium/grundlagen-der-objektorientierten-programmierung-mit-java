package org.iu.dsgopj102101.uebungsprojekt.terminueberwachung;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

public class TeamKalender {

    private List<Termin> termine = new LinkedList<>();

    public void addTermin(Termin termin) {
        termine.add(termin);
    }

    public void removeTermin(Termin termin) {
        termine.remove(termin);
    }

    public List<Termin> getTermine() {
        return termine;
    }

    public void importFromCsv(String csvFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(csvFile));

        String line = br.readLine(); // Read first line to skip header row
        while ((line = br.readLine()) != null) {
            String[] values = line.split(";");
            if (values.length != 8) {
                continue;
            }

            try {
                int id = Integer.parseInt(values[0]);
                String betreff = values[1];
                int mitarbeiterId = Integer.parseInt(values[2]);
                float dauerInStunden = Float.parseFloat(values[3]);

                String startDatum = values[4];
                String startZeit = values[5];
                String endDatum = values[6];
                String endZeit = values[7];
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

                Termin termin = Termin.createIfPlausible(
                        id,
                        betreff,
                        mitarbeiterId,
                        dauerInStunden,
                        sdf.parse(startDatum + " " + startZeit),
                        sdf.parse(endDatum + " " + endZeit)
                );
                addTermin(termin);
            } catch (IllegalArgumentException | ParseException | TerminImplausibleException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Termin termin : termine) {
            sb.append(termin.toString()).append("\n");
        }
        return sb.toString();
    }
}
