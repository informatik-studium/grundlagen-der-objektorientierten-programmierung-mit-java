package org.iu.dsgopj102101.uebungsprojekt.terminueberwachung;

public class TerminImplausibleException extends Exception {

    public TerminImplausibleException(String message) {
        super(message);
    }
}
