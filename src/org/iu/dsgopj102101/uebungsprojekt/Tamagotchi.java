package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.tamagotchi.*;

import java.util.Scanner;

/**
 * Eine Version des Tamagotchi-Spiels.
 *
 * @author Torben Wetter
 */
public class Tamagotchi {

    /**
     * Main-Methode.
     * Ruft nur die Funktion zum Starten des Spiels auf.
     *
     * @param args Kommandozeilenargumente (werden nicht verwendet)
     */
    public static void main(String[] args) {
        spielStarten();
    }

    /**
     * Startet das Spiel.
     * Der Benutzer kann ein Tier auswaehlen, Aktionen auf diesem ausfuehren und muss dafuer sorgen, dass es nicht verhungert.
     */
    private static void spielStarten() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welches Tier moechtest Du benutzen?");
        System.out.print("Pferd (1), Hund (2), Katze (3) oder Fisch (4)? ");
        int tierAuswahl = scanner.nextInt();
        Tier tier;
        if (tierAuswahl == 1) {
            tier = new Pferd();
            System.out.println("Du benutzt jetzt ein Pferd.");
        } else if (tierAuswahl == 2) {
            tier = new Hund();
            System.out.println("Du benutzt jetzt einen Hund.");
        } else if (tierAuswahl == 3) {
            tier = new Katze();
            System.out.println("Du benutzt jetzt eine Katze.");
        } else if (tierAuswahl == 4) {
            tier = new Fisch();
            System.out.println("Du benutzt jetzt einen Fisch.");
        } else {
            System.out.println("Ungueltige Eingabe! Starte das Spiel erneut...");
            return;
        }

        boolean verhungert = false;
        while (!verhungert) {
            System.out.println("--------------------");
            System.out.println("Welche Aktion moechtest Du durchfuehren?");
            System.out.print("Aktivitaet (1), Fuettern (2) oder Hungerstatus abfragen (3)? ");
            int aktionAuswahl = scanner.nextInt();

            if (tier.calculateMinutenSeitLetzterFuetterung() > 5.0) {
                verhungert = true;
                continue;
            }

            if (aktionAuswahl == 1) {
                tier.aktivitaetDurchfuehren();
            } else if (aktionAuswahl == 2) {
                tier.fuettern();
                System.out.println("Das Tier wurde gefuettert.");
            } else if (aktionAuswahl == 3) {
                System.out.println("Das Tier hat" + (tier.hatHunger() ? "" : " keinen") + " Hunger");
            } else {
                System.out.println("Ungueltige Eingabe! Versuche es erneut...");
            }
        }

        System.out.println("Du hast das Tier zu lange nicht gefuettert! Game Over.");
    }
}
