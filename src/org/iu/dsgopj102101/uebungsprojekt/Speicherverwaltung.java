package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.speicherverwaltung.Speicherraum;

public class Speicherverwaltung {

    public static void main(String[] args) {
        Speicherraum<String> raum = new Speicherraum<>(String.class, 5, 5, 5);
        raum.set("Hallo Welt!", 3, 3, 1);
        System.out.println(raum.get(3, 3, 1));
        System.out.println(raum.get(2, 4, 0));
    }
}
