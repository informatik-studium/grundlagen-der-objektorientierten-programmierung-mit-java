package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.klausur.fahrtenbuch.*;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Klausur {

    private static final Fahrtenbuch fahrtenbuch = new Fahrtenbuch();

    private static List<IFahrtenbuchObjekt> fahrtenbuchObjekte = new LinkedList<>() {
        {
            add(new LKW("1", "XX-XX-XXXX", new Date(), 0, 10, 5));
            add(new PKW("2", "XX-XX-XXXX", new Date(), 0, "Marke", "Modell", "Farbe"));
            add(new Auflieger("3", "XX-XX-XXXX"));
        }
    };

    public static void main(String[] args) {
        System.out.println("Willkommen in Ihrer Fahrtenbuch-Software!");
        Scanner scanner = new Scanner(System.in);

        boolean beenden = false;
        while (!beenden) {
            System.out.println("--------------------");
            System.out.println("Was möchten Sie als nächstes tun?");
            System.out.print("Eintrag ergänzen (1), Einträge auflisten (2) oder Programm beenden (3) ");
            int auswahl = scanner.nextInt();
            scanner.nextLine();
            switch (auswahl) {
                case 1:
                    System.out.println("Um welches Fahrtenbuch-Objekt handelt es sich? ");
                    // TODO: Abfragen und fahrtenbuch.eintragErgaenzen(...); aufrufen
                    break;
                case 2:
                    fahrtenbuch.eintraegeAuflisten();
                    break;
                case 3:
                    beenden = true;
                    break;
                default:
                    System.out.println("Falsche Eingabe!");
                    break;
            }
        }

        scanner.close();
    }
}
