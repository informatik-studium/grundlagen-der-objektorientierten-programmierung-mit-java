package org.iu.dsgopj102101.uebungsprojekt.tamagotchi;

import java.util.Date;

public abstract class Tier {

    private static final double MINUTEN_BIS_HUNGER = 2.0;

    private Date zuletztGefuettert;

    public Tier() {
        fuettern();
    }

    public void fuettern() {
        this.zuletztGefuettert = new Date();
    }

    public abstract void aktivitaetDurchfuehren();

    public boolean hatHunger() {
        return calculateMinutenSeitLetzterFuetterung() > MINUTEN_BIS_HUNGER;
    }

    public double calculateMinutenSeitLetzterFuetterung() {
        long letzteFuetterungMillis = this.zuletztGefuettert.getTime();
        long jetztMillis = System.currentTimeMillis();
        return (jetztMillis - letzteFuetterungMillis) / 1000.0 / 60.0;
    }
}
