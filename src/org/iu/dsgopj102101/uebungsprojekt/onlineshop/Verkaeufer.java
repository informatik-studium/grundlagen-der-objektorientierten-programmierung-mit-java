package org.iu.dsgopj102101.uebungsprojekt.onlineshop;

public class Verkaeufer extends Kunde {

    private String umsatzsteuerId;

    public Verkaeufer(String name, String email, String umsatzsteuerId) {
        super(name, email);

        this.umsatzsteuerId = umsatzsteuerId;
    }

    public String getUmsatzsteuerId() {
        return umsatzsteuerId;
    }

}
