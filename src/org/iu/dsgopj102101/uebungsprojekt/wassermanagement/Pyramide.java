package org.iu.dsgopj102101.uebungsprojekt.wassermanagement;

public class Pyramide extends Wasserbecken {

    private float a;
    private float h;

    public Pyramide(float a, float h) {
        this.a = a;
        this.h = h;
    }

    @Override
    public float volumen() {
        return (float) (Math.pow(this.a, 2) * this.h / 3.0);
    }

}
