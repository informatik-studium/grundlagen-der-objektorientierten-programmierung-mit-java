package org.iu.dsgopj102101.uebungsprojekt.wassermanagement;

public class Kegelstumpf extends Wasserbecken {

    private float radius1;
    private float radius2;
    private float hoehe;

    public Kegelstumpf(float radius1, float radius2, float hoehe) {
        this.radius1 = radius1;
        this.radius2 = radius2;
        this.hoehe = hoehe;
    }

    @Override
    public float volumen() {
        return (float) (Math.PI / 3 * this.hoehe * (Math.pow(radius1, 2) + radius1 * radius2 + Math.pow(radius2, 2)));
    }

}
