package org.iu.dsgopj102101.uebungsprojekt.wassermanagement;

public class Wasserauffangbecken extends Wasserbecken {

    private float laenge;
    private float breiteOben;
    private float breiteUnten;
    private float hoehe;

    public Wasserauffangbecken(float laenge, float breiteOben, float breiteUnten, float hoehe) {
        this.laenge = laenge;
        this.breiteOben = breiteOben;
        this.breiteUnten = breiteUnten;
        this.hoehe = hoehe;
    }

    @Override
    public float volumen() {
        return this.laenge * (this.breiteOben + this.breiteUnten) * 0.5f * this.hoehe;
    }

    public static float minHoeheErmitteln(float laenge, float breiteOben, float breiteUnten, float minWassermenge) {
        float minVolumen = minWassermenge / 1000.0f;
        float minHoehe = minVolumen / laenge / (breiteOben + breiteUnten) / 0.5f;
        return minHoehe;
    }

}
