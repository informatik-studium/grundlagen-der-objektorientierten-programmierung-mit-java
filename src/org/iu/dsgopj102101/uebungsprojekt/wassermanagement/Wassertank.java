package org.iu.dsgopj102101.uebungsprojekt.wassermanagement;

public class Wassertank extends Wasserbecken {

    private float durchmesser;
    private float hoehe;

    public Wassertank(float durchmesser, float hoehe) {
        this.durchmesser = durchmesser;
        this.hoehe = hoehe;
    }

    @Override
    public float volumen() {
        return (float) (Math.PI * Math.pow(this.durchmesser / 2, 2) * this.hoehe);
    }

}
