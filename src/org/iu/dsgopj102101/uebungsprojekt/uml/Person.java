package org.iu.dsgopj102101.uebungsprojekt.uml;

public class Person {

    private String vorname;
    private String nachname;
    private Auto auto;

    public Person(String vorname, String nachname) {
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public void fahre(Auto auto) {
        this.setAuto(auto);
        this.getAuto().drive();
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getVorname() {
        return vorname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    public Auto getAuto() {
        return auto;
    }

}
