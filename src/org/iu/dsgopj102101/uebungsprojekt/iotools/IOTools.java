/*
    Copyright (C) 1999  (Jens Scheffler)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
package org.iu.dsgopj102101.uebungsprojekt.iotools;

/*
 * Quelle:
 *   Scheffler (2018): "Klasse IOTools" heruntergeladen von Internetseite www.grundkurs-java.de: "Grundkurs Programmieren in Java.
 *   Benötigte Software und Installationsanleitungen für das Buch." Online unter: https://www.grundkurs-java.de/index.php/tools-und-anleitungen,
 *   Hyperlink zur Datei: https://www.grundkurs-java.de/images/dateiablage/anleitungen/Prog1Tools_2018.zip
 *   Stand 23.06.2021 [Hinweis: aus der ZIP Datei wurde die Datei IOTools.java entnommen].
 *
 * Achtung:
 *   Die Klasse von Scheffler wurde von Kuhlen um die Methoden writeToFile und readFile ergänzt.
 *   Außerdem wurde die Klasse von Wetter formatiert und auf Vordermann gebracht.
 */

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Diese Klasse stellt einige einfache Methoden zum Einlesen von der Tastatur zur Verfügung. Es werden diverse Werte
 * von der Tastatur eingelesen, die jeweils durch ein Leerzeichen, einen Tab oder ein Zeilenendezeichen getrennt sein
 * müssen.
 *
 * @author Jens Scheffler
 * @version 1.01 Spezialfassung für <i>Programmieren 1 in Java</i>
 */
public class IOTools {

    private IOTools() {
    } // somit kann die Klasse nicht instanziiert werden!

    private static final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private static StringTokenizer eingabe;
    private static String zeichenkette;

    /**
     * Löscht alles, was sich in der momentanen Zeile befindet. Das heißt, es wird der Eingabe bis zum Zeilenende keine
     * Beachtung mehr geschenkt.
     */
    public static void flush() {
        eingabe = null;
    }

    /* Private Methode, die den Tokenizer füllt. Dies ist übrigens eine von zwei Methoden, die die Klasse zum Absturz
     bringen kann... */
    private static void init() {
        zeichenkette = null;
        if (eingabe != null && eingabe.hasMoreTokens()) return;
        while (eingabe == null || !eingabe.hasMoreTokens())
            eingabe = new StringTokenizer(readLine());
    }

    /* Private Methode, die eine Fehlermeldung ausgibt */
    private static void error(Exception e, String prompt) {
        System.out.println("Eingabefehler " + e);
        System.out.println("Bitte Eingabe wiederholen...");
        System.out.print(prompt);
    }

    /**
     * Liest eine ganze Textzeile von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden, geschieht
     * dies durch den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return die eingelesene Zeile.
     */
    /* Dies ist die zweite Methode, die die Klasse zum Absturz bringen kann.*/
    public static String readLine(String prompt) {
        flush();
        String erg = "";
        System.out.print(prompt);
        try {
            erg = in.readLine();
        } catch (IOException e) {
            System.err.println("" + e + "\n Programm abgebrochen...\n");
            System.exit(1);
        }
        if (erg == null) {
            System.err.println("Dateiende erreicht.\nProgramm abgebrochen...\n");
            System.exit(1);
        }
        return erg;
    }

    /**
     * Liest eine {@code Int}-Zahl von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden,
     * geschieht dies durch den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return die eingelesene Zahl.
     */
    public static int readInteger(String prompt) {
        int erg;
        System.out.print(prompt);
        init();
        while (true) {
            try {
                erg = Integer.parseInt(eingabe.nextToken());
            } catch (NumberFormatException e) {
                error(e, prompt);
                init();
                continue;
            }
            return erg;
        }
    }

    /**
     * Liest eine {@code Long}-Zahl von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden,
     * geschieht dies durch den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return die eingelesene Zahl.
     */
    public static long readLong(String prompt) {
        long erg;
        System.out.print(prompt);
        init();
        while (true) {
            try {
                erg = Long.parseLong(eingabe.nextToken());
            } catch (NumberFormatException e) {
                error(e, prompt);
                init();
                continue;
            }
            return erg;
        }
    }

    /**
     * Liest eine {@code Double}-Zahl von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden,
     * geschieht dies durch den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return die eingelesene Zahl.
     */
    public static double readDouble(String prompt) {
        double erg;
        System.out.print(prompt);
        init();
        while (true) {
            try {
                erg = Double.parseDouble(eingabe.nextToken());
            } catch (NumberFormatException e) {
                error(e, prompt);
                init();
                continue;
            }
            return erg;
        }
    }

    /**
     * Liest eine {@code Float}-Zahl von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden,
     * geschieht dies durch den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return die eingelesene Zahl.
     */
    public static float readFloat(String prompt) {
        float erg;
        System.out.print(prompt);
        init();
        while (true) {
            try {
                erg = Float.parseFloat(eingabe.nextToken());
            } catch (NumberFormatException e) {
                error(e, prompt);
                init();
                continue;
            }
            return erg;
        }
    }

    /**
     * Liest eine {@code Short}-Zahl von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden,
     * geschieht dies durch den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return die eingelesene Zahl.
     */
    public static short readShort(String prompt) {
        short erg;
        System.out.print(prompt);
        init();
        while (true) {
            try {
                erg = Short.parseShort(eingabe.nextToken());
            } catch (NumberFormatException e) {
                error(e, prompt);
                init();
                continue;
            }
            return erg;
        }
    }

    /**
     * Liest einen boolschen Wert von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden,
     * geschieht dies durch den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return der eingelesene Wert.
     */
    public static boolean readBoolean(String prompt) {
        String try_this = readString(prompt);
        while (!try_this.equals("true") && !try_this.equals("false"))
            try_this = readString();
        return try_this.equals("true");
    }

    /**
     * Liest ein Textwort von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden, geschieht dies
     * durch den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return das eingelesene Wort.
     */
    public static String readString(String prompt) {
        System.out.print(prompt);
        init();
        return eingabe.nextToken();
    }

    /**
     * Liest ein Zeichen von der Tastatur ein. Soll vorher eine Eingabeaufforderung gemacht werden, geschieht dies durch
     * den Parameter. Dieser kann jedoch auch wegfallen.
     *
     * @param prompt eine eventuelle Eingabeaufforderung
     * @return das eingelesene Zeichen.
     */
    public static char readChar(String prompt) {
        char erg;
        System.out.print(prompt);
        if (zeichenkette == null || zeichenkette.length() == 0)
            zeichenkette = readString("");
        erg = zeichenkette.charAt(0);
        zeichenkette = (zeichenkette.length() > 1) ? zeichenkette.substring(1) : null;
        return erg;
    }

    /**
     * Liest eine ganze Textzeile von der Tastatur ein.
     *
     * @return die eingelesene Zeile.
     */
    public static String readLine() {
        return readLine("");
    }

    /**
     * Liest eine {@code Int}-Zahl von der Tastatur ein.
     *
     * @return die eingelesene Zahl.
     */
    public static int readInteger() {
        return readInteger("");
    }

    /**
     * Liest eine {@code Long}-Zahl von der Tastatur ein.
     *
     * @return die eingelesene Zahl.
     */
    public static long readLong() {
        return readLong("");
    }

    /**
     * Liest eine {@code Double}-Zahl von der Tastatur ein.
     *
     * @return die eingelesene Zahl.
     */
    public static double readDouble() {
        return readDouble("");
    }

    /**
     * Liest eine {@code Short}-Zahl von der Tastatur ein.
     *
     * @return die eingelesene Zahl.
     */
    public static short readShort() {
        return readShort("");
    }

    /**
     * Liest eine {@code Float}-Zahl von der Tastatur ein.
     *
     * @return die eingelesene Zahl.
     */
    public static float readFloat() {
        return readFloat("");
    }

    /**
     * Liest ein Zeichen von der Tastatur ein.
     *
     * @return das eingelesene Zeichen
     */
    public static char readChar() {
        return readChar("");
    }

    /**
     * Liest ein Textwort von der Tastatur ein.
     *
     * @return das eingelesene Wort.
     */
    public static String readString() {
        return readString("");
    }

    /**
     * Liest einen boolschen Wert von der Tastatur ein.
     *
     * @return das eingelesene Wort.
     */
    public static boolean readBoolean() {
        return readBoolean("");
    }

    /**
     * Wandelt eine {@code Double}-Zahl in einen String um. Bei der üblichen Umwandlung von {@code Double}-Werten in
     * einen String findet eine Rundung statt. So wird etwa die Zahl 0.1, obwohl intern nicht darstellbar, dennoch auf
     * dem Bildschirm ausgegeben. Diese Methode umgeht die Rundung.
     */
    public static String toString(double d) {
        if (Double.isInfinite(d) || Double.isNaN(d))
            return "" + d;
        return (new BigDecimal(d)).toString();
    }


    /**
     * Funktion zum Schreiben von Dateiinhalten.
     *
     * @param file   Vollständiger Pfad inklusive Dateinamen zu der Datei, die geschrieben wird. <b>Achtung:</b> Der zugehörige Ordner muss angelegt sein.
     * @param line   Zeile, die in die Datei geschrieben wird.
     * @param append Logischer Schalter. Wenn dieser "true" ist, wird die übergebene Zeile angehängt.
     * @author Kuhlen
     */
    public static void writeToFile(String file, String line, boolean append) {
        try {
            BufferedWriter output = new BufferedWriter(new FileWriter(file, append));
            if (append) {
                output.append(line);
            } else {
                output.write(line);
            }
            output.newLine();
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Funktion zum Auslesen von Dateiinhalten.
     *
     * @param file vollständiger Pfad zur Datei, die zu lesen ist
     * @return Liste von Strings, jede Zeile wird als String ausgegeben
     * @author Kuhlen
     */
    public static List<String> readFile(String file) {
        List<String> lines = new ArrayList<>();
        try {
            BufferedReader bReader = new BufferedReader(new FileReader(file));
            String currentLine = bReader.readLine();
            while (currentLine != null) {
                // Füge die aktuell gelesene Zeile ein
                lines.add(currentLine);
                // Lese die nächste Zeile
                currentLine = bReader.readLine();
            }
            bReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lines;
    }
}
