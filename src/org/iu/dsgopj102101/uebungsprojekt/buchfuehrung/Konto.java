package org.iu.dsgopj102101.uebungsprojekt.buchfuehrung;

import java.util.LinkedList;
import java.util.List;

public class Konto {

    private String name;
    private List<Buchung> buchungen;

    public Konto(String name) {
        this.name = name;
        this.buchungen = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public List<Buchung> getBuchungen() {
        return buchungen;
    }

    public void addBuchung(Buchung buchung) {
        buchungen.add(buchung);
    }

    public double kontostandBerechnen() {
        double summe = 0;
        for (Buchung buchung : buchungen) {
            summe += buchung.getBetrag();
        }
        return summe;
    }
}
