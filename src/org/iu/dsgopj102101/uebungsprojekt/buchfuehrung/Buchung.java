package org.iu.dsgopj102101.uebungsprojekt.buchfuehrung;

import java.util.Date;

public class Buchung {

    private Date datum;
    private double betrag;
    private String text;

    public Buchung(double betrag, String text) {
        this.datum = new Date();
        this.betrag = betrag;
        this.text = text;
    }

    private Buchung(Date datum, double betrag, String text) {
        this.datum = datum;
        this.betrag = betrag;
        this.text = text;
    }

    public Date getDatum() {
        return datum;
    }

    public double getBetrag() {
        return betrag;
    }

    public String getText() {
        return text;
    }

    public static Buchung mitDatumErstellen(Date datum, double betrag, String text) {
        return new Buchung(datum, betrag, text);
    }
}
