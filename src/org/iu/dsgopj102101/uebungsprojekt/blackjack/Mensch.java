package org.iu.dsgopj102101.uebungsprojekt.blackjack;

import java.util.Random;

public class Mensch extends Spieler {

    private static final int NAME_LENGTH = 5;
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    private final String name;
    private boolean ausgestiegen;

    public Mensch(String name) {
        super();
        this.name = name;
        this.ausgestiegen = false;
    }

    public String getName() {
        return this.name;
    }

    public boolean istAusgestiegen() {
        return ausgestiegen;
    }

    public void aussteigen() {
        this.ausgestiegen = true;
    }

    public static String generateName() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int i = 0;
        while (i++ < NAME_LENGTH) {
            sb.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }
        String name = sb.toString();
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}
