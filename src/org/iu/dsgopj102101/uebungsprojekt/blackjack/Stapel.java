package org.iu.dsgopj102101.uebungsprojekt.blackjack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class Stapel extends Stack<Karte> {

    private static final byte KARTEN_PRO_WERT = 4;

    public Stapel() {
        this.kartenZufaelligGenerieren();
    }

    private void kartenZufaelligGenerieren() {
        List<Karte> karten = new ArrayList<>();
        for (byte wert : new byte[]{2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11}) {
            for (byte i = 0; i < KARTEN_PRO_WERT; i++) {
                karten.add(new Karte(wert));
            }
        }
        Collections.shuffle(karten);
        this.addAll(karten);
    }

    @Override
    public synchronized Karte pop() {
        Karte karte = super.pop();
        if (this.size() == 0) {
            this.kartenZufaelligGenerieren();
        }
        return karte;
    }
}
