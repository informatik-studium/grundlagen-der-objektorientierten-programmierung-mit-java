package org.iu.dsgopj102101.uebungsprojekt.blackjack;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Spiel {

    private final Stapel stapel;
    private final Maschine maschine;
    private final int anzahlMenschen;
    private final List<Mensch> menschen;

    public Spiel(int anzahlMenschen) {
        this.stapel = new Stapel();
        this.maschine = new Maschine();
        this.anzahlMenschen = anzahlMenschen;
        this.menschen = new ArrayList<>();
        for (int i = 0; i < Math.max(anzahlMenschen, 1); i++) {
            this.menschen.add(new Mensch(Mensch.generateName()));
        }
    }

    public void spielen() {
        System.out.println("Das BlackJack-Spiel beginnt!");
        System.out.println("Es spiel" + (this.anzahlMenschen == 1 ? "t " : "en ") + this.anzahlMenschen + " Spieler gegen eine Maschine.");
        String menschenNamen = this.menschen.stream().map(Mensch::getName).collect(Collectors.joining(", "));
        System.out.println((this.anzahlMenschen == 1 ? "Der" : "Die") + " Spieler heiß" + (this.anzahlMenschen == 1 ? "t " : "en ") + menschenNamen + ".");

        int runde = 1;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("--------------------");
            System.out.println("Runde " + runde++ + ":");

            for (Mensch mensch : this.menschen) {
                System.out.println("--------------------");
                System.out.print("Spieler " + mensch.getName() + ": ");
                System.out.print("Du hast " + mensch.getPunkte() + " Punkte auf der Hand");
                if (mensch.istAusgestiegen()) {
                    System.out.println(" und bist bereits ausgestiegen.");
                    continue;
                }
                System.out.println(".");

                System.out.print("Möchtest Du eine Karte ziehen? (ja/...) ");
                String eingabe = scanner.next().toLowerCase();
                if (!eingabe.equals("ja")) {
                    mensch.aussteigen();
                    System.out.println("Du bist ausgestiegen.");
                    continue;
                }

                Karte karte = this.stapel.pop();
                mensch.ziehen(karte);
                System.out.print("Du hast eine " + karte.getWert() + " gezogen");
                System.out.println(" und nun " + mensch.getPunkte() + " Punkte auf der Hand.");

                if (mensch.getPunkte() == 21) {
                    System.out.println("Damit hast Du gewonnen!!!");
                    System.out.println("Die Maschine kam gar nicht erst zum Zug.");
                    return;
                }
            }
        } while (this.menschen.stream().anyMatch(mensch -> !mensch.istAusgestiegen() && mensch.getPunkte() < 21));

        System.out.println("--------------------");

        List<Mensch> menschenImSpiel = this.menschen.stream().filter(mensch -> mensch.getPunkte() < 21).collect(Collectors.toList());
        if (menschenImSpiel.size() == 0) {
            System.out.println("Alle Spieler haben die 21 überschritten. Damit hat die Maschine gewonnen!");
            return;
        }

        Mensch besterMensch = menschenImSpiel.stream().max(Comparator.comparing(Mensch::getPunkte)).get();
        System.out.println("Der beste Spieler ist " + besterMensch.getName() + " mit " + besterMensch.getPunkte() + " Punkten.");
        System.out.println("Nun ist die Maschine an der Reihe.");

        do {
            if (this.maschine.getPunkte() > besterMensch.getPunkte()) {
                System.out.println("Damit hat die Maschine gewonnen!");
                return;
            }

            Karte karte = this.stapel.pop();
            this.maschine.ziehen(karte);
            System.out.print("Die Maschine hat eine " + karte.getWert() + " gezogen");
            System.out.println(" und nun " + this.maschine.getPunkte() + " Punkte auf der Hand.");
        } while (this.maschine.getPunkte() <= 21);

        System.out.println("Die Maschine hat die 21 überschritten. Damit hat " + besterMensch.getName() + " gewonnen!");
    }
}
