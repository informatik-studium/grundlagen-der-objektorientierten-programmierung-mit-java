package org.iu.dsgopj102101.uebungsprojekt.blackjack;

public class Karte {

    private final byte wert;

    public Karte(byte wert) {
        this.wert = wert;
    }

    public byte getWert() {
        return wert;
    }
}
