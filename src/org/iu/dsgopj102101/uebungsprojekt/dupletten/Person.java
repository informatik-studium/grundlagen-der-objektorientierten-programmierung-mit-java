package org.iu.dsgopj102101.uebungsprojekt.dupletten;

import java.util.Objects;

public class Person {

    private final String vorname, nachname, geburtsdatum, anschrift, nutzername;

    public Person(String vorname, String nachname, String geburtsdatum, String anschrift, String nutzername) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.geburtsdatum = geburtsdatum;
        this.anschrift = anschrift;
        this.nutzername = nutzername;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public String getGeburtsdatum() {
        return geburtsdatum;
    }

    public String getAnschrift() {
        return anschrift;
    }

    public String getNutzername() {
        return nutzername;
    }

    @Override
    public boolean equals(Object o) {
        // True if the reference matches
        if (this == o) return true;

        // False if Person o is null or the objects' classes differ
        if (o == null || getClass() != o.getClass()) return false;

        // True if all attributes match
        Person person = (Person) o;
        return person.getVorname().equals(this.vorname)
                && person.getNachname().equals(this.nachname)
                && person.getGeburtsdatum().equals(this.geburtsdatum)
                && person.getAnschrift().equals(this.anschrift)
                && person.getNutzername().equals(this.nutzername);
    }

    @Override
    public int hashCode() {
        // Ensure that equal objects have equal hash codes
        return Objects.hash(getVorname(), getNachname(), getGeburtsdatum(), getAnschrift(), getNutzername());
    }
}
