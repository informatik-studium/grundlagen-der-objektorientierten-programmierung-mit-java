package org.iu.dsgopj102101.uebungsprojekt.spiel;

public class Lampe extends Spielobjekt {

    public Lampe(Spieler spieler) {
        super(spieler);
    }

    @Override
    public void benutzen() {
        System.out.println("Durch Spieler " + getSpieler().getName() + " wurde es hell.");
    }
}
