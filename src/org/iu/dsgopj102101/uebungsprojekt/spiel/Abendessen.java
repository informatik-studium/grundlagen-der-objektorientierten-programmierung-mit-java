package org.iu.dsgopj102101.uebungsprojekt.spiel;

public class Abendessen extends Spielobjekt {

    public Abendessen(Spieler spieler) {
        super(spieler);
    }

    @Override
    public void benutzen() {
        System.out.println("Spieler " + getSpieler().getName() + " wurde satt.");
    }
}
