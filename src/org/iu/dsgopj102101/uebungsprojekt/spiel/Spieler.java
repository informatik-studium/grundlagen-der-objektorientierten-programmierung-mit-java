package org.iu.dsgopj102101.uebungsprojekt.spiel;

public class Spieler {

    private final String name;

    public Spieler(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
