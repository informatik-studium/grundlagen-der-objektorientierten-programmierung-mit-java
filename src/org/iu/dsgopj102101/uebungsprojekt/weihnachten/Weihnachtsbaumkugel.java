package org.iu.dsgopj102101.uebungsprojekt.weihnachten;

public class Weihnachtsbaumkugel {

    private double durchmesser;

    public Weihnachtsbaumkugel(double durchmesser) {
        this.durchmesser = durchmesser;
    }

    public double berechneVolumen() {
        return 4.0 / 3.0 * Math.PI * Math.pow(this.durchmesser / 2, 3);
    }

    public double berechneOberflaeche() {
        return 4.0 * Math.PI * Math.pow(this.durchmesser / 2, 2);
    }

    public double getDurchmesser() {
        return durchmesser;
    }

    public void setDurchmesser(double durchmesser) {
        this.durchmesser = durchmesser;
    }

}
